//
//  ViewController.m
//  ObjCHookingApp
//
//  Created by Phil Goo Kang on 19/07/2017.
//  Copyright © 2017 Phil Goo Kang. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://onoffmix.philgookang.com"]]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)webViewDidFinishLoad:(UIWebView *)_webView {
    
    JSContext * javascriptContext = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    javascriptContext[@"window"][@"jsToast"] = ^(){
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self.view makeToast:@"토스트!"];
        });
    };
    javascriptContext[@"window"][@"jsNotification"] = ^(){
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
            content.title = [NSString localizedUserNotificationStringForKey:@"알림" arguments:nil];
            content.body = [NSString localizedUserNotificationStringForKey:@"이것은 알림 입니다." arguments:nil];
            
            // Deliver the notification in five seconds.
            UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
            UNNotificationRequest* request = [UNNotificationRequest requestWithIdentifier:@"FiveSecond" content:content trigger:trigger];
            
            // Schedule the notification.
            UNUserNotificationCenter * center = [UNUserNotificationCenter currentNotificationCenter];
            [center addNotificationRequest:request withCompletionHandler:nil];
        });
        
    };
}

@end
