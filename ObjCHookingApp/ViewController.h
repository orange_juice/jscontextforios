//
//  ViewController.h
//  ObjCHookingApp
//
//  Created by Phil Goo Kang on 19/07/2017.
//  Copyright © 2017 Phil Goo Kang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import "UIView+Toast.h"
#import <UserNotifications/UserNotifications.h>


@interface ViewController : UIViewController <UIWebViewDelegate> {
    IBOutlet UIWebView *webView;
}

@end

