//
//  AppDelegate.h
//  ObjCHookingApp
//
//  Created by Phil Goo Kang on 19/07/2017.
//  Copyright © 2017 Phil Goo Kang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

